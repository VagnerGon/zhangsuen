#include <opencv2//core//core.hpp>
#include <opencv2//highgui//highgui.hpp>

#include <stdlib.h>
#include <ctime>

#include "P2P.h"

#define PIXEL 255
#define APAGADO 0

#define P0 matriz[1][0]
#define P1 matriz[0][0]
#define P2 matriz[0][1]
#define P3 matriz[0][2]
#define P4 matriz[1][2]
#define P5 matriz[2][2]
#define P6 matriz[2][1]
#define P7 matriz[2][0]

int contaVizinhos(unsigned char matriz[3][3]) {
	
	return (P0 + P1 + P2 + P3 + P4 + P5 + P6 + P7)/PIXEL;
}

int contaTransicoes (unsigned char matriz[3][3]) {

	unsigned char sequencia[] = {P2, P3, P4, P5, P6, P7, P0, P1, P2};   

	int numeroTransicao = 0;
	for(int i = 1; i < 10; i++) {
		if (sequencia[i-1] == APAGADO && sequencia[i] == PIXEL)
			numeroTransicao++;
	}

	return numeroTransicao;
}

unsigned char ZhangSuen(unsigned char matriz[3][3], int passo) {

	if (matriz[1][1] == APAGADO)
		return APAGADO;

	int nVizinhos = contaVizinhos(matriz);
	if(nVizinhos < 2 || nVizinhos > 6)
		return PIXEL;

	int nTransicoes = contaTransicoes(matriz);
	if(nTransicoes != 1)
		return PIXEL;

	int vizinhaca;
	
	if(passo == 1){
		vizinhaca = P2 * P4 * P6;
		if(vizinhaca != APAGADO)
			return PIXEL;

		return P4 * P6 * P0 == APAGADO ? APAGADO : PIXEL;
	}
	//Passo 2
	vizinhaca = P2 * P4 * P0;
	if(vizinhaca != APAGADO)
		return PIXEL;

	return P2 * P6 * P0 == APAGADO ? APAGADO : PIXEL;
}

void getMatixData(unsigned char* imageData, int linha, int coluna, int largura, int altura, unsigned char matriz[3][3]) {
	

	for(int a = 0; a < 3; a++) {
		for(int b = 0; b < 3; b++) {            
			
			int xn = linha + a - 1;
			int yn = coluna + b - 1;
								
			int index = xn * largura + yn;

			if(index < 0)
				matriz[a][b] = 0;
			matriz[a][b] = imageData[index];			
		}
	}
}

bool verificaDiferenca(unsigned char* image_data, unsigned char* nova_imagem, int largura, int altura) {
	
	for(int linha = 1; linha < altura-1; linha++) {
		for(int coluna = 1; coluna < largura-1; coluna++) {
			
			int index = linha*largura + coluna;
			if(image_data[index] != nova_imagem[index])
				return true;
		}
	}
	return false;
}

int main() {

	clock_t start,finish;

	start = clock();

	IplImage *imagem = cvLoadImage("in.png", CV_LOAD_IMAGE_GRAYSCALE);

	unsigned char* imageData = (unsigned char*) imagem->imageData;	

	unsigned char matriz[3][3];

	bool diferenca = true;

	int a = 0;

	unsigned char* esqueleto = (unsigned char*) malloc(sizeof(unsigned char) * imagem->height * imagem->widthStep);	

	memcpy(esqueleto, imageData, sizeof(unsigned char) * imagem->height * imagem->widthStep);

	while (diferenca) {

		unsigned char* novaImagem = (unsigned char*) malloc(sizeof(unsigned char) * imagem->height * imagem->widthStep);	
		//Imagem que sera alterada dentro dos passos
		unsigned char* intermediaria = (unsigned char*) malloc(sizeof(unsigned char) * imagem->height * imagem->widthStep);	
		memcpy(intermediaria, esqueleto, sizeof(unsigned char) * imagem->height * imagem->widthStep);

		for(int passo = 1; passo <= 2; passo++){
			for(int linha = 0; linha < imagem->height; linha++) {
				for(int coluna = 0; coluna < imagem->widthStep; coluna++) {
				
					//Verifica se nao esta nas bordas
					if(linha == 0 || coluna == 0 || linha == imagem->height-1 || coluna == imagem->widthStep-1) 
						novaImagem[linha*imagem->widthStep + coluna] = intermediaria[linha*imagem->widthStep + coluna];
					else{

						getMatixData(intermediaria, linha, coluna, imagem->widthStep, imagem->height, matriz);
						novaImagem[linha*imagem->widthStep + coluna] = ZhangSuen(matriz, passo);				
					}
				}
			}		
			memcpy(intermediaria, novaImagem, sizeof(unsigned char) * imagem->height * imagem->widthStep);
		}		
		free(intermediaria);

		diferenca = verificaDiferenca(esqueleto, novaImagem, imagem->widthStep, imagem->height);
		 
		free(esqueleto);
		esqueleto = (unsigned char*) malloc(sizeof(unsigned char) * imagem->height * imagem->widthStep);	

		memcpy(esqueleto, novaImagem, sizeof(unsigned char) * imagem->height * imagem->widthStep);
		free(novaImagem);	
		
		a++;		
	}

	finish = clock() - start;
	double interval = finish / (double)CLOCKS_PER_SEC; 
	printf("%d - tempo : %f ms \n", a, interval * 1000);

	IplImage* imageResult = cvCreateImageHeader(cvSize(imagem->widthStep,imagem->height), IPL_DEPTH_8U, 1);
	cvSetData(imageResult,esqueleto,imagem->widthStep);
	cvShowImage("Afinada", imageResult);

	cv::imwrite("saida.png", (cv::Mat)imageResult);

	free(esqueleto);

	cvWaitKey(100000);
	
	return 0;
}
